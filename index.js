const csv = require('csv-parser');
const fs = require('fs');

const lastFlag = process.argv.length - 1;
const language = process.argv[lastFlag];

let currentJSON = {};

fs.createReadStream('csv/' + language + '.csv')
  .pipe(csv())
  .on('data', (row) => {
    // specialcase
    if (language === 'en') {
      const keyVal = row[""];
      const curVal = row[" " + language + " "];
      currentJSON[keyVal] = curVal;
    } else {
      const keyVal = row.en;
      const curVal = row[language];
      currentJSON[keyVal] = curVal;
    }
  })
  .on('end', (res) => {
    fs.writeFile('json/' + language + '.json', JSON.stringify(currentJSON, null, 4), 'utf8', () => {
      console.log('✅:', language);
    });
  })
  .on('error', (error) => {
    console.error(error);
  });

