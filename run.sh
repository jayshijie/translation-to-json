#!/bin/bash
EN_SHEET_URL="http://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=1751942243"
ZH_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=1615936644"
VN_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=709491827"
TH_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=0"
ID_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=353228789"
MS_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=1648239336"
JP_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=933589813"
KR_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=1023146450"
ES_SHEET_URL="https://docs.google.com/spreadsheets/d/1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI/export?format=csv&id=1m9dKtBKc641vlH7926xn7V3GUR8WQ2aefaK3iwSFjNI&gid=1540067744"
BASEDIR=$(dirname "$0")

rm -rf csv/
rm -rf json/
rm -rf output.txt
mkdir csv
mkdir json

# en
curl -L --max-time 10 --output "$BASEDIR/csv/en.csv" "$EN_SHEET_URL"
node index.js en
enjson=$(cat "$BASEDIR/json/en.json")

# zh
curl -L --max-time 10 --output "$BASEDIR/csv/zh.csv" "$ZH_SHEET_URL"
node index.js zh
zhjson=$(cat "$BASEDIR/json/zh.json")

# vn
curl -L --max-time 10 --output "$BASEDIR/csv/vn.csv" "$VN_SHEET_URL"
node index.js vn
vnjson=$(cat "$BASEDIR/json/vn.json")

# th
curl -L --max-time 10 --output "$BASEDIR/csv/th.csv" "$TH_SHEET_URL"
node index.js th
thjson=$(cat "$BASEDIR/json/th.json")

# id
curl -L --max-time 10 --output "$BASEDIR/csv/id.csv" "$ID_SHEET_URL"
node index.js id
idjson=$(cat "$BASEDIR/json/id.json")

# ms
curl -L --max-time 10 --output "$BASEDIR/csv/ms.csv" "$MS_SHEET_URL"
node index.js ms
msjson=$(cat "$BASEDIR/json/ms.json")

# jp
curl -L --max-time 10 --output "$BASEDIR/csv/jp.csv" "$JP_SHEET_URL"
node index.js jp
jpjson=$(cat "$BASEDIR/json/jp.json")

# kr
curl -L --max-time 10 --output "$BASEDIR/csv/kr.csv" "$KR_SHEET_URL"
node index.js kr
krjson=$(cat "$BASEDIR/json/kr.json")

# es
curl -L --max-time 10 --output "$BASEDIR/csv/es.csv" "$ES_SHEET_URL"
node index.js es
esjson=$(cat "$BASEDIR/json/es.json")

# COPY CODE TO MINI GAME LANGUAGE FILE
cat <<EOF >./output.js
(miniGame['en'] = $enjson),
(miniGame['zh'] = $zhjson),
(miniGame['vn'] = $vnjson),
(miniGame['th'] = $thjson),
(miniGame['id'] = $idjson),
(miniGame['ms'] = $msjson),
(miniGame['jp'] = $jpjson),
(miniGame['kr'] = $krjson),
(miniGame['es'] = $esjson);
EOF

echo "✅: COPY output.txt to your translation"
