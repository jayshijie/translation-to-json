# Translation file to json

## Project setup

```
npm install
```

### To start

```
npm run start
```

### Demo

Before you run the script, making sure you set the permission to public during the build process. (And disable public access after you had done the export)

See example below: 

![](./google-permission.gif)

![demo](./demo.gif)